# PimaOnline ThemePack

The PimaOnline Themepack is a web development toolkit for building online courses within the D2L Brightspace learning environment. The theme pack offers multiple and interchangeable themes.

## Version

2.1.3

## What's included

### Dependencies

- Git
- Node.js
  - Gulp
    - Gulp-sass
- jQuery 3.3.1 or newer

## Stylesheets

### SCSS

Stylesheets are created using individual scss modules, then minified and compiled into main.css

#### **\_grid.scss**

Contains all styles that makeup a page’s grid. The two main web layout models used are CSS Grid and Flexbox.

#### **\_typography.scss**

Contains all typography/font styles, excluding type colors.

#### **\_widgets.scss**

Widgets are individual components, each serving a different purpose such as adding functionality or changing content layout. Pages are assembled using widgets which fall within the page’s grid.

#### **\_variables.scss**

Contains scss variables for Pima Community College’s brand colors. This scss module is included in all other scss partials and therefore not included in main.scss.

#### **\_legacy.scss**

Contains legacy (deprecated) code for courses that are still in use but use code developed prior to the latest version.

#### **main.scss**

Used to include all individual scss files, and to compile them to a single css file.

### CSS

CSS is created by compiling from scss files. Note: Do not edit css, instead edit scss and compile.

#### **main.css**

Contains compiled and merged rules of our scss styles.

#### **routes.css**

Used to route main.css, font-awesome, and include character set for delivery to individual themes.

### JavaScript

Contains all JavaScript and jQuery code.

### Themes

Themes are individually built stylesheets with distinct styles which are developed for specific programs. Themes are built to automatically pull this themepack’s styles (from main.css), meaning you should link only the theme’s stylesheet to the head of your document.

## Plugins

Plugins are 3rd party assets that are imported and used to add functionality.

## Authors

Center for Learning Technology at Pima Community College

## License

Code is released under the MIT license.
