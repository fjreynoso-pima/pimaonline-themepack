// This script uses a forEach loop to place a custom stylesheet into every shadow dom (#shadow-root) component,
/// then while still in the loop runs scripts through each component

window.parent.addEventListener("load", function () {

  // Find all shadow-dom components and create an array
  let sRoots = document.querySelectorAll("d2l-html-block");

  // Loop through array
  sRoots.forEach((each) => {

    // Set the root element of each shadow component to a variable
    let sRoot = each.shadowRoot.querySelector(".d2l-html-block-rendered");
    
    // Create link element
    let link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = "https://d2l.pima.edu/shared/webdev/course-files/plugins/global-homepage-overrides/global-homepage-overrides.css";
    
    // Append link element to root element
    sRoot.append(link);

    // BEGIN SCRIPTS *note: still going through forEach loop

    // PCC Quicklinks
    /// if the element has both scroll-right/left
    if (each.shadowRoot.getElementById("pcc-ql-scroll-right") && each.shadowRoot.getElementById("pcc-ql-scroll-left"))  {

      const buttonRight = each.shadowRoot.getElementById("pcc-ql-scroll-right");
      const buttonLeft = each.shadowRoot.getElementById("pcc-ql-scroll-left");
      const mediaQuery = window.matchMedia("(min-width: 413px)");
    
      buttonRight.onclick = function () {
        if (mediaQuery.matches) {
          each.shadowRoot.getElementById("pcc-ql-container").scrollLeft += 490;
        } else {
          each.shadowRoot.getElementById("pcc-ql-container").scrollLeft += 300;
        }
      };
    
      buttonLeft.onclick = function () {
        if (mediaQuery.matches) {
          each.shadowRoot.getElementById("pcc-ql-container").scrollLeft -= 490;
        } else {
          each.shadowRoot.getElementById("pcc-ql-container").scrollLeft -= 300;
        }
      };
    };


  }) 
});