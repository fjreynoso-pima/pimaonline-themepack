// This script uses a forEach loop to place a custom stylesheet into every shadow dom (#shadow-root) component

window.parent.addEventListener("load", function () {

  // Find all shadow-dom components and create an array
  let sRoots = document.querySelectorAll("d2l-html-block");

  // Loop through the array
  sRoots.forEach((each) => {

    // Set the root-element of each shadow-component to a variable
    let sRoot = each.shadowRoot.querySelector(".d2l-html-block-rendered");
    
    // Create link element
    let link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = "https://d2l.pima.edu/shared/webdev/course-files/plugins/preview-banner/preview-banner.css";
    
    // Append link element to root element
    sRoot.append(link);

  });

});

// Click any of the left navigation links to rerun script above.
// This is done to refresh the CSS inserted into the shadow component
let refresh = document.querySelectorAll(".d2l-le-TreeAccordionItem-anchor");

refresh.forEach((each_) => {
  each_.addEventListener("click", () => {

    setTimeout(() => {
      let d2lBlock = document.querySelectorAll("d2l-html-block");

      // Loop through the array
      d2lBlock.forEach((_each) => {

        // Set the root-element of each shadow-component to a variable
        let innerD2lBlock = _each.shadowRoot.querySelector(".d2l-html-block-rendered");
        
        // Create link element
        let _link = document.createElement("link");
        _link.type = "text/css";
        _link.rel = "stylesheet";
        _link.href = "https://d2l.pima.edu/shared/webdev/course-files/plugins/preview-banner/preview-banner.css";
        
        // Append link element to root element
        innerD2lBlock.append(_link);

      });      

    }, 1000);

  })
});