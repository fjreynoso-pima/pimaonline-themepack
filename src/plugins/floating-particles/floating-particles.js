// JavaScript Document
window.addEventListener("DOMContentLoaded", function() {

  let particleAmount = 75;
  const get_body = document.querySelector("body");
  
  const canvas_elem = document.createElement("canvas");
  const ctx = canvas_elem.getContext("2d");
  
  canvas_elem.id = "animated-bg";
  canvas_elem.width = window.innerWidth;
  canvas_elem.height = window.innerHeight;
  get_body.prepend(canvas_elem);
  
  const particleArray = [];
  let particleFill = "rgba(255, 255, 255, .08)";

  window.addEventListener("resize", function () {
    canvas_elem.width = window.innerWidth;
    canvas_elem.height = window.innerHeight;
  });

  class Particle {
    constructor() {
		this.x = Math.random() * ( (canvas_elem.width - 5) - 5 ) + 5;
		this.y = Math.random() * canvas_elem.height;
		this.size = Math.random() * (30 - 10) + 10;
		this.speedX = 0;
		this.speedY = Math.random() * (5 - 2) + 2 ;
    }
    update() {
      if (this.y + this.size > canvas_elem.height || this.y + this.size < 0) {
        this.speedY = -this.speedY;
      }
      this.x += this.speedX;
      this.y += this.speedY;
    }
    draw() {
      ctx.fillStyle = particleFill;
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.size, 0, Math.PI * 2);
      ctx.fill();
    }
  }

  function init() {
    for (let i = 0; i < particleAmount; i++) {
      particleArray.push(new Particle());
    }
  }
  init();

  function handleParticles() {
    for (let i = 0; i < particleArray.length; i++) {
      particleArray[i].update();
      particleArray[i].draw();
    }
  }

  function animate() {
    ctx.clearRect(0, 0, canvas_elem.width, canvas_elem.height);
    handleParticles();
    requestAnimationFrame(animate);
  }
  animate();  
  
// end event listener
});