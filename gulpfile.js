// Import gulp plugins
const gulp = require("gulp");
const pug = require("gulp-pug");
const sass = require("gulp-sass")(require("sass"));

// Default
gulp.task("default", async () => {
    return console.log("Hello human");
});

// Move js, imgs and plugins from src to dist
gulp.task("copy-js", async () => {
    return gulp.src("src/js/*.js")
    .pipe(gulp.dest("dist/js"));
});

gulp.task("copy-imgs", async () => {
    return gulp.src("src/img/**")
    .pipe(gulp.dest("dist/img"));
});

gulp.task ("copy-plugins", async () => {
    return gulp.src("src/plugins/**")
    .pipe(gulp.dest("dist/plugins"));
});

// Compile main SCSS
gulp.task("sass-main", async () => {
    return gulp.src("src/scss/**/*.scss").pipe(sass({outputStyle: "compressed"}).on("error", sass.logError)).pipe(gulp.dest("dist/css"));
});

// Compile webdocs pug files
gulp.task("pug-docs", async () => {
    return gulp.src("docs/src/pug/**/*.pug")
    .pipe(pug({ pretty: true }))
    .pipe(gulp.dest("docs/dist/html"));
});

// Compile webdocs SCSS
gulp.task("sass-docs", async () => {
    return gulp.src("docs/src/scss/main.scss")
    .pipe(sass({outputStyle: "compressed"}).on("error", sass.logError))
    .pipe(gulp.dest("docs/dist/css"));
});

// Move webdocs js from src to dist
gulp.task("js-docs", async () => {
    return gulp.src("docs/src/js/*.js")
    .pipe(gulp.dest("docs/dist/js"));
})