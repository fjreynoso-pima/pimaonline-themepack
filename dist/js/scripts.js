/// @description Main JS file for PimaOnline Themepack
/// @dependencies jQuery 3.3.1 or later

window.addEventListener("DOMContentLoaded", function () {

  // Grid 
  if (document.getElementById("second-column") && document.getElementById("third-column")) {
    document.getElementsByTagName("body")[0].id = "three-column";

  } else if (document.getElementById("second-column") && !document.getElementById("column-widget")) {
    document.getElementsByTagName("body")[0].id = "two-column";

  } else if (document.getElementById("column-widget")) {
    document.getElementsByTagName("body")[0].id = "two-col-widget";

  } else if (document.getElementById("video-wrapper")) {
    document.getElementsByTagName("body")[0].id = "video-grid";

  } else {
    document.getElementsByTagName("body")[0].id = "one-column";
  }

  // JS to add role and aria-label to content-wrapper, second-column, and third-column
  let cwAttrib = document.querySelector("#content-wrapper");
  cwAttrib.setAttribute("role", "main");

  if (document.querySelector("#second-column")) {
    let scAttrib = document.querySelector("#second-column");
    scAttrib.setAttribute("role", "region");
    scAttrib.setAttribute("aria-label", "second column");
  }

  if (document.querySelector("#third-column")) {
    let tcAttrib = document.querySelector("#third-column");
    tcAttrib.setAttribute("role", "region");
    tcAttrib.setAttribute("aria-label", "third column");
  }

  // Remove role="presentation" attr
  if (document.querySelectorAll('[role="presentation"]')) {
    let rolePres = document.querySelectorAll('[role="presentation"]');
    rolePres.forEach((roleElem) => roleElem.removeAttribute("role"));
  }

  // Helper JS for Responsive Tables
  function initResponsiveTables() {
    var tables = document.querySelectorAll(".display, .display-lg")

    for (var t = 0; t < tables.length; t++) {

      var headertext = [],
        headers = tables[t].querySelectorAll(".display table th, table.display th, .display-lg table th, table.display-lg th"),
        tablebody = tables[t].querySelector(".display table tbody, table.display tbody, .display-lg table tbody, table.display-lg tbody");

      for (var i = 0; i < headers.length; i++) {
        var current = headers[i];
        headertext.push(current.textContent.replace(/\r?\n|\r/, ""));
      }
      for (var y = 0, row; row = tablebody.rows[y]; y++) {
        for (var j = 0, col; col = row.cells[j]; j++) {
          col.setAttribute("data-th", headertext[j]);
        }
      }
    }
  }
  initResponsiveTables();

  // Remove inline CSS from tables
  const discardAttributes = (element, ...attributes) =>
    attributes.forEach((attribute) => element.removeAttribute(attribute));

  document.querySelectorAll("table, thead, tbody, tr, th, td").forEach((elem) => {
    // Function parameters: discardAttributes(element, attribute1, attribute2, attribute3, ...)
    discardAttributes(elem, "cellspacing", "cellpadding", "width", "style");
  });

  // Append scripts-async.js to DOM
  const addScript = (script) => {
    let newScript = document.createElement("script");
    newScript.setAttribute("src", script);
    
    document.body.append(newScript);
  }
  
  if(document.querySelector("[onclick]")) {
    addScript("https://d2l.pima.edu/shared/webdev/course-files/js/jumpTo.js");
  }

  // Show/Hide Toggle Button for Accordion
  $('.toggle-btn').on('click', function () {
    $(this).next().slideToggle(200);
  });

  // Show/Hide for Footnotes and Footer
  $('.toggle-footnotes').on('click', function () {
    $(this).next().slideToggle(200);
    $(this).text($(this).text() == '[Hide Footnotes]' ? '[Show Footnotes]' : '[Hide Footnotes]');
  });

  // Toggle Button's Arrow Right Points Down on Click
  $('.arrow-right').on('click', function () {
    $(this).toggleClass('arrow-down');
  });

  // Fancybox
  $("[data-fancybox]").fancybox({
    idleTime: false,
    topRatio: 0.1,
    helpers: {
      title: {
        type: 'inside'
      },
    }
  });

  /*------WIDGET SCRIPTS------ */
  /* TOOLTIP ------ */
  // Allows Screen readers to toggle a tooltip on click and to say if the tooltip is collapsed or expanded.

  $(".tooltip").click(function () {
    $(this).children(".tip-hover").toggle();
    if ($(this).children(".tip-hover").is(':visible')) {
      $(this).attr('aria-expanded', 'true');
      $(this).removeClass('hidden');
    } else {
      $(this).attr('aria-expanded', 'false');
      $(this).addClass('hidden');
    }
  });

  var start = 999;

  $('.tooltip').each(function (i) {
    $(this).css('z-index', start--);
  });

  $(".tooltip .video-container").parent().css("width", "450px");

}); // end Document-ready